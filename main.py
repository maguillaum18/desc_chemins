import osmnx as ox
import networkx as nx
import geopandas as gpd
import crseg.utils as u
import crseg.segmentation as cs
import json
import crgraph as cg
import SegmentationReader as sr



overpass = 0
latitude = 45.77417
longitude = 3.09028
G1 = u.Util.get_osm_data(latitude, longitude,1000, overpass)

# prepare network by removing unwanted ways
G1 = cs.Segmentation.prepare_network(G1)
# build an undirected version of the graph
undirected_G = ox.utils_graph.get_undirected(G1)
#working on non-oriented graph
G1 = undirected_G

#adding new attributes to the graph
cg.prepare_graph_OSM(G1)

# segment it using topology and semantic
seg = cs.Segmentation(undirected_G, C0 = 2, C1 = 2, C2 = 4, max_cycle_elements = 10)

seg.process()
#exporting to json
seg.to_json_all("crossroads.json")



#List of all the intersection and branch
seg_reader = sr.SegmentationReader("crossroads.json")

#Create our new model
G = cg.create_graph_model(G1,seg_reader)
#Add edges
cg.add_edges(G1,G,seg_reader)

#visualization of the OSMnx graph
ox.plot_graph(G1,bgcolor='w',node_color='k', node_size=5,node_alpha=0.7, node_edgecolor='b', node_zorder=5, edge_color='r', edge_linewidth=2,edge_alpha=0.8,show=True)

#visualization of the new model

ox.plot_graph(G,bgcolor='w',node_color='k', node_size=5,node_alpha=0.8, node_edgecolor='b', node_zorder=5, edge_color='r', edge_linewidth=2,edge_alpha=0.8,show=True)




