""" Create segmentationReader object to have a list of Segmented crossroad and Segmented branch from a json file product by Segmentation object """

import json

class SegmentedCrossroad():

    def __init__(self, inner_nodes, border_nodes, edges_by_nodes,cross_id,coord, branches):
        self.inner_nodes = inner_nodes
        self.border_nodes= border_nodes
        self.edges_by_nodes = edges_by_nodes
        self.branches = branches
        self.cross_id = cross_id
        self.coord = coord

    def isCrossroad(self,cid):
        if( (cid in self.inner_nodes) or (cid in self.border_nodes) ):
             return True
        else:
             return False


class SegmentedBranch():

    def __init__(self, id, inner_nodes, border_nodes, edges_by_nodes,coord):
        self.id = id
        self.inner_nodes = inner_nodes
        self.border_nodes= border_nodes
        self.edges_by_nodes = edges_by_nodes
        self.coord = coord
        self.nb_visit = 0

    def visited(self):
        self.nb_visit += 1


    def isBranch(self,bid):
        if( (bid in self.inner_nodes) or (bid in self.border_nodes) ):
             return True
        else:
             return False


class SegmentationReader():

    def __init__(self, path):
        json_file = open(path)
        self.data = json.load(json_file)
        json_file.close()
        self.crossroads = []
        self.cid = 1 #each crossroad has a unique ID : cid, 1st crossroad ID = 1

        # If there is more than one crossroad
        if isinstance(self.data[0], list):
            for crossroad in self.data:
                self.crossroads.append(self.__read_crossroad_data(crossroad,self.cid))
                self.cid += 1
        else:
            self.crossroads.append(self.__read_crossroad_data(self.data))

    def getNumberOfCrossRoads(self):
        return len(self.crossroads)

    def getCrossroads(self):
        return self.crossroads

    def getCrossroads_by_id(self,cid):
        for a in self.crossroads:
            if(a.cross_id == cid):
                return a

    def __read_crossroad_data(self, data,cid):
        crossroad = None
        branches = []
        id = 1
        for el in data:
            inner_nodes =       el['nodes']['inner']
            border_nodes =      el['nodes']['border']
            edges_by_nodes =    el['edges_by_nodes']
            coord = el['coordinates']
            if el['type'] == 'crossroad':
                crossroad = SegmentedCrossroad(inner_nodes, border_nodes, edges_by_nodes,cid,coord, None)
            else:
                branches.append(SegmentedBranch(id, inner_nodes, border_nodes, edges_by_nodes,coord))
                id += 1
        crossroad.branches = branches
        return crossroad