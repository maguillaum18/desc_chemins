""" Functions to create a new graph model G from an OSMnx graph and a list of segmented intersections"""

import osmnx as ox
import networkx as nx

def prepare_graph_OSM(G1):
    """
    add color, branch ID and crossroad ID attribute to each node in the OSMnx graph,
    default value = 0
    """
    cross_id = {}
    color = {}
    branch = {}
    for k in G1.nodes():
        cross_id[k] = 0
        color[k] = 0
        branch[k] = 0
    nx.set_node_attributes(G1, cross_id, name='cid')
    nx.set_node_attributes(G1,color,name='color')
    nx.set_node_attributes(G1,branch,name='branch')



def clear_color(G1):
    """
    reset the color attribute for each node
    """
    for node in G1.nodes():
        G1.nodes[node]['color'] = 0


def create_graph_model(G1,seg_reader):
    """
    create and return a new graph G from OSMnx graph G1 and segmentationReader object
    add every intersections as nodes and border_nodes and inner_nodes of the intersections as node attributes
    """
    G = nx.MultiDiGraph()
    crossroads = seg_reader.getCrossroads()

    for node in G1.nodes():
        if(G1.nodes[node]["cid"] == 0):
            for cross in crossroads:
                if(cross.isCrossroad(node) and (node not in G.nodes  )):
                    G.add_node(cross.cross_id,inner_nodes = cross.inner_nodes,border_nodes=cross.border_nodes)
                    #add coordinates of the intersection = coordinates of the 1st inner_node
                    if(cross.inner_nodes):
                        G.nodes[cross.cross_id]['x'] = cross.coord[str(cross.inner_nodes[0])]['x']
                        G.nodes[cross.cross_id]['y'] = cross.coord[str(cross.inner_nodes[0])]['y']
                    #if the intersection doesnt have any inner_nodes
                    else:
                        G.nodes[cross.cross_id]['x'] = cross.coord[str(cross.border_nodes[0])]['x']
                        G.nodes[cross.cross_id]['y'] = cross.coord[str(cross.border_nodes[0])]['y']

                    #set the crossroad id and branch id attributes in the OSMnx graph
                    dico_crossroad = {}
                    for i in cross.inner_nodes:
                        dico_crossroad[i] = cross.cross_id
                    for j in cross.border_nodes:
                        dico_crossroad[j] = cross.cross_id
                    nx.set_node_attributes(G1, dico_crossroad, name='cid')

                    dico_branche = {}
                    for branche in cross.branches:
                        for i in branche.inner_nodes:
                            dico_branche[i] = branche.id
                        for j in branche.border_nodes:
                            dico_branche[j] = branche.id
                    nx.set_node_attributes(G1, dico_branche, name='branch')
    G.graph['crs'] = 4326
    return G


def select_start(G1,G,branche,crossroad):
    """
    return a list of border_nodes' for one branch of an intersection
    these nodes are potential start for the BFS
    """
    start = []
    for node in branche.border_nodes:
        if(G1.nodes[node]['cid'] != crossroad.cross_id):
            start.append(node)
    return start



def BFS(G1,G,start,cross):
    """
    execute BFS
    start : a node of one of the intersection cross' branch
    stop : first node of the adjacent intersection is visited OR graph boundaries reached
    return : next_cross -firt node of the adjacent crossroad(OR 0 if the limit is reached before),
             branche    -destination branch ID
             data       -list of visited nodes
    """
    L = []
    data = []
    L.append(start)
    G1.nodes[L[0]]['color'] = 1 #color 1 = node visited
    next_cross = 0
    #color all the nodes of the starting intersection
    for node in cross.inner_nodes:
        G1.nodes[node]['color'] = 1
    for node in cross.border_nodes:
        G1.nodes[node]['color'] = 1
    while(L and next_cross == 0):
        courant = L.pop(0)
        data.append(courant)
        if(G1.nodes[courant]['cid'] != cross.cross_id and G1.nodes[courant]['branch'] != 0):
            branche = G1.nodes[courant]['branch']
        if(G1.nodes[courant]['cid'] != 0 and G1.nodes[courant]['cid'] != cross.cross_id):
            next_cross =  courant
        else:
            for neighbor in G1.neighbors(courant):
                if(G1.nodes[neighbor]['color'] == 0):
                    L.append(neighbor)
                    G1.nodes[neighbor]['color'] = 1

    return(next_cross,branche,data)




def add_edges(G1,G,seg_reader):
    """ add edges in G graph between connected crossroads
        with 2 edge attributes: branches = pair of branch ID of the 2 connected crossroads, data = all the nodes on the way
    """
    crossroads = seg_reader.getCrossroads()
    for crossroad in crossroads:
        for branche in crossroad.branches:
            #avoid adding the same edge in the 2 directions
            if(branche.nb_visit < 1):
                start = select_start(G1,G,branche,crossroad)
                if(start):
                    branche.visited()
                    clear_color(G1) #reset all the colored nodes before BFS
                    j = BFS(G1,G,start[0],crossroad)
                    if(j[0]): #find the adjacent intersection
                        cross = crossroads[G1.nodes[j[0]]['cid']-1]
                        for k in cross.branches: #find the destination branch of the adjacent intersection
                            if(k.id == j[1]):
                                k.visited()
                                G.add_edge(crossroad.cross_id,G1.nodes[j[0]]['cid'],branches=[branche.id,k.id],data=j[2])